const gulp = require("gulp")
const ts = require("gulp-typescript")
const replace = require('gulp-replace')
const merge = require('merge2')
const lazypipe = require('lazypipe')
const removeEmptyLines = require('gulp-remove-empty-lines')

module.exports = function () {

  var parameters = require('../../typeorm-entitysharer.json')

  if (typeof (parameters) === 'undefined') {
    console.log('Parameters not found')
    throw new Error('Parameters not found')
  }
  const decoratorsWhitelist = parameters.decoratorsWhitelist || []
  const importsWhitelist = parameters.importsWhitelist || []
  const extendsWhitelist = parameters.extendsWhitelist || []

  if (typeof (parameters.tsconfigPath) === 'undefined' || parameters.tsconfigPath === '') throw new Error('Could not find tsconfig file')

  const tsProjectClient = ts.createProject(parameters.tsconfigPath)
  const tsProjectServer = ts.createProject(parameters.tsconfigPath)
  const tsProjectErrors = ts.createProject({ declaration: true })
  const tasks = [];

  /**
   * If catched values is whitelisted, returns the full match, else return empty string
   * @param <string[]> whitelist
   * @param <string> fullMatch : the string containing the catched value
   * @param <string> catched: catched value, coming from regexp
   * @return <string> fullMatch | ''
   */
  const replaceWhitelist = (whitelist, fullMatch, catched, str = '') => whitelist.indexOf(catched) !== -1 ? fullMatch : str

  // compose replace function for imports strings
  const replaceDecorators = (fullMatch, catched) => replaceWhitelist(decoratorsWhitelist, fullMatch, catched)

  // compose replace function for decorators strings
  const replaceImports = (fullMatch, catched) => replaceWhitelist(importsWhitelist, fullMatch, catched)

  // compose replace function for class inheritance strings
  const replaceExtends = (fullMatch, catched) => extendsWhitelist.some(item => fullMatch.includes(item)) ? fullMatch : "{"

  // Loading ts stream
  const loadTs = lazypipe()
    .pipe(gulp.src, 'src/**/*.ts')

  /**
   * Ts stripping stream
   * - remove non-whitelisted imports
   * - remove non-whitelisted decorators
   * - remove extends ... keywords
   */
  var stripTs = lazypipe()
    .pipe(loadTs)
    .pipe(replace, /^import [\sa-zA-Z0-9-_{},]+ from '(.*)';?/gm, replaceImports)
    .pipe(replace, /extends [^{]+ {/g, replaceExtends)
    .pipe(replace, /@([a-zA-Z0-9-_]+)\s*\([^)]*\)/g, replaceDecorators)
    .pipe(removeEmptyLines)
  
  /**
   * Build for client-side
   * Remove all extends ... { & some decorators in class so client .ts will not contain this
   */
  gulp.task('build:client', () => {
    // strip & transpile ts
    const tsResult = stripTs()
      .pipe(tsProjectClient())

    // save stripped ts & js to client repo
    return merge([
      stripTs().pipe(gulp.dest('dist/client')),
      tsResult.js.pipe(gulp.dest('dist/client'))
    ]);
  });

  tasks.push('build:client');

  /**
   * Build for server side 
   * Save ts, .d.ts & .js
   */
  gulp.task('build:server', () => {
    const tsResult = loadTs()
      .pipe(tsProjectServer())

    // save original ts & transpiled js to server repo
    return merge([
      loadTs().pipe(gulp.dest('dist/server')),
      tsResult.js.pipe(gulp.dest('dist/server'))
    ]);
  });
  tasks.push('build:server');


  /**
   * Build Errors
   */

  if (typeof (parameters.errorsPath) === 'undefined' || parameters.errorsPath === '') {
    gulp.task('build:errors', () => {
      var tsResult = gulp.src(parameters.errorsPath)
        .pipe(tsProjectErrors());

      return merge([ // Merge the two output streams, so this task is finished when the IO of both operations is done.
        tsResult.dts.pipe(gulp.dest('dist/errors')),
        tsResult.js.pipe(gulp.dest('dist/errors'))
      ]);
    });
    tasks.push('build:errors');
  }

  // default will run both build
  gulp.task('default', tasks)

  gulp.start('default')

}
